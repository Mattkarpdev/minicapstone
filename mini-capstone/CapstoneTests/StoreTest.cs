using Capstone.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CapstoneTests
{
    [TestClass]
    public class StoreTest
    {
        [TestMethod]
        public void InventoryArrayTest()
        {
            //Arrange
            Store testObject = new Store();

            List<Item_Candy> result = new List<Item_Candy>();

            //Act (done in arrange above)
            Item_Candy candyList = new Item_Candy();

                candyList.Id = "CH";
                candyList.Name = "matt";
                candyList.Price = 5.0M;
                candyList.Qty = 10;
                candyList.Type = "CE";
                candyList.Wrapper = "T";
                result.Add(candyList);

            Item_Candy[] list = result.ToArray();
            list = testObject.InventoryArray();

            //Assert
            Assert.IsNotNull(list);
        }

        [TestMethod]

        public void IsSoldOut()
        {
            Store testObject = new Store();

            testObject.SoldOut();

            Assert.AreEqual(false, testObject.SoldOut());
        }

        [TestMethod]

        public void IdentificationTest()
        {
            Store testObject = new Store();

            
            Assert.AreEqual(true, testObject.Identification("C1"));
            Assert.AreEqual(false, testObject.Identification("A2"));

        }

        [TestMethod]

        public void AddToCartTest()
        {
            Store testObject = new Store();

            List<Item_Candy> test = testObject.AddToCart(2, "C1");
            foreach(Item_Candy item in test)
            {
                Assert.AreEqual("Snuckers Bar", item.Name);
                Assert.AreEqual(2.70M, item.TotalCost);
            }

            //Assert.AreEqual(true, test);


        }

        [TestMethod]
        public void SufficientStockTest()
        {
            Store testObject = new Store();

            Assert.AreEqual(true, testObject.SufficientStock(5));
            Assert.AreEqual(false, testObject.SufficientStock(101));
        }

        [TestMethod]
        public void SufficientBalanceTest()
        {
            Store testObject = new Store();
            testObject.Balance = 45;

            Assert.AreEqual(true, testObject.SufficientQty(5,"C1"));
            Assert.AreEqual(false, testObject.SufficientQty(70, "C1"));
        }

        [TestMethod]
        public void Add1Test()
        {
            Store testObject = new Store();

            Assert.AreEqual(1, testObject.Add1());
          
        }
       
        [TestMethod]
        public void Add5Test()
        {
            Store testObject = new Store();

            Assert.AreEqual(5, testObject.Add5());

        }

        [TestMethod]
        public void Add10Test()
        {
            Store testObject = new Store();

            Assert.AreEqual(10, testObject.Add10());

        }

        [TestMethod]
        public void Add20Test()
        {
            Store testObject = new Store();

            Assert.AreEqual(20, testObject.Add20());

        }

        [TestMethod]
        public void Add50Test()
        {
            Store testObject = new Store();

            Assert.AreEqual(50, testObject.Add50());

        }

        [TestMethod]
        public void Add100Test()
        {
            Store testObject = new Store();

            Assert.AreEqual(100, testObject.Add100());

        }

        [TestMethod]
        public void PaymentCostTest()
        {
            Store testObject = new Store();

            testObject.Balance = 5;

            Assert.AreEqual(2.30M, testObject.PaymentCost(2, "C1"));
        }

        [TestMethod]
        public void ItemTotalTest()
        {
            Store testObject = new Store();
            Item_Candy candyList = new Item_Candy();
            
            candyList.Id = "C1";
            candyList.Name = "matt";
            candyList.Price = 5.0M;
            candyList.Qty = 10;
            candyList.Type = "CH";
            candyList.Wrapper = "T";

            testObject.cart.Add(candyList);

           

            Assert.AreEqual(50 , testObject.ItemTotal());

        }

        [TestMethod]
        public void ChangeAmountTest()
        {
            Store testObject = new Store();

            testObject.Balance = 5;

            Assert.AreEqual(5, testObject.ChangeAmount());
        }

        [TestMethod]
        public void BillChangeTest()
        {
            Store testObject = new Store();

            Assert.AreEqual("(0) Twenties, (0) Tens, (1) Fives, (0) Ones, (0) Quarters, (0) Dimes, (0) Nickels, (0) Pennies", testObject.BillChange(5));

         }

        [TestMethod]
        public void BgeTest()
        {
            {
                // arrange
                using (var stream = new MemoryStream())
                using (var writer = new StreamWriter(stream)) ;
                
                    // act
                    FileIO.WriteLog(writer);

                    // assert
                    string actual = Encoding.UTF8.GetString(stream.ToArray());
                    Assert.AreEqual("some expected output", actual);
                
            }
        }
    }
}
