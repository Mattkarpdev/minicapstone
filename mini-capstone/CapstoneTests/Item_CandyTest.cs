﻿using System;
using System.Collections.Generic;
using System.Text;
using Capstone.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CapstoneTests
{
    [TestClass]
    public class Item_CandyTest
    {
        [TestMethod]

        public void Item_CandyConstructorTest()
        {
            Item_Candy testObject = new Item_Candy("CH", "C1", "Chunky", "T", 1.50M, 5, 7.50M);

            Assert.AreEqual("CH", testObject.Type);
            Assert.AreEqual("C1", testObject.Id);
            Assert.AreEqual("Chunky", testObject.Name);
            Assert.AreEqual("T", testObject.Wrapper);
            Assert.AreEqual(1.50M, testObject.Price);
            Assert.AreEqual(5, testObject.Qty);
            Assert.AreEqual(7.50M, testObject.TotalCost);


        }
    }
}
