﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Classes
{
    public class Item_Candy : IComparable
    {
        public string Type { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Wrapper { get; set; }
        public decimal Price { get; set; }
        public int Qty { get; set; }
        public decimal TotalCost
        {
            get
            {
                return Price * Qty;
            }
        }
        public Item_Candy()
        {

        }

        public Item_Candy(string type, string id,string name,string wrapper,decimal price, int quantity, decimal TotalCost)
        {
            Type = type;
            Id = id;
            Name = name;
            Wrapper = wrapper;
            Price = price;
            Qty = quantity;
        }
        public int CompareTo(object item)
        {
            Item_Candy temp = (Item_Candy)item;
            return string.Compare(Id, temp.Id);

        }
    }
}
