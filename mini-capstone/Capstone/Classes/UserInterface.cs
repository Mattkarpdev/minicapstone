﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Linq;

namespace Capstone.Classes
{
    class UserInterface
    {
        private Store store = new Store();
        /// <summary>
        /// Provides all communication with human user.
        /// 
        /// All Console.Readline() and Console.WriteLine() statements belong 
        /// in this class.
        /// 
        /// NO Console.Readline() and Console.WriteLine() statements should be 
        /// in any other class
        /// 
        /// </summary>


        public void Run()
        {
            bool done = false;

            while (!done)
            {
                DisplayMenu();

                string userResponse = Console.ReadLine();

                switch (userResponse)
                {
                    case "1":
                        ShowInventory();
                        break;
                    case "2":
                        MakeSale();
                        break;
                    case "3":
                        done = true;
                        break;
                    default:
                        Console.WriteLine("Please enter a valid choice.");
                        break;

                }

            }

        }
        private void ShowInventory()
        {
            Console.WriteLine("--------------------------------------------------------");
            Console.WriteLine("Id    Name               Wrapper     Qty           Price");
            Console.WriteLine("--------------------------------------------------------");
            Item_Candy[] inv = store.InventoryArray();
            Array.Sort(inv);
            string temp = "";
            foreach (Item_Candy item in inv)
            {
                if (item.Qty == 0)
                {
                    temp = "Sold Out";
                }
                else
                {
                    temp = item.Qty.ToString();
                }
                Console.WriteLine($"{item.Id.PadRight(4)}  {item.Name.PadRight(17)}  {item.Wrapper.PadRight(10)}  {temp.PadRight(12)}  {item.Price:C2}");
            }
        }
        private void MakeSale()
        {
            bool isDone = false;

            while (!isDone)
            {
                DisplaySubMenu();
                Console.WriteLine($"Current Customer Balance: {store.Balance:C2}");
                string userResponse = Console.ReadLine();

                switch (userResponse)
                {
                    case "1":
                        TakeMoney();
                        break;
                    case "2":
                            if (store.Balance > 0)
                            {
                                SelectProducts();
                            }
                            else
                            {
                                Console.WriteLine("Please add money to your balance.");
                            }
                        break;
                    case "3":
                        CompleteSale();
                        isDone = true;
                        break;
                    default:
                        Console.WriteLine("Please enter a valid choice.");
                        break;

                }


            }

        }

        public void TakeMoney()
        {
            bool isDone = false;

            while (!isDone)
            {
                DisplaySubTakeMoney();
                Console.WriteLine($"Current Customer Balance: {store.Balance:C2}");
                string userResponse = Console.ReadLine();
                
                switch (userResponse)
                {
                    case "1":
                        store.Add1();
                        break;
                    case "2":
                        store.Add5();
                        break;
                    case "3":
                        store.Add10();
                        break;
                    case "4":
                        store.Add20();
                        break;
                    case "5":
                        store.Add50();
                        break;
                    case "6":
                        store.Add100();
                        break;
                    case "7":
                        isDone = true;
                        break;
                    default:
                        Console.WriteLine("Please enter a valid choice.");
                        isDone = true;
                        break;
                }
                if (store.ExceedsBalance == true)
                {
                    Console.WriteLine("Exceeds a max balance of 1000");
                }
            }
        }
        private void SelectProducts()
        {
            ShowInventory();
            Console.WriteLine("Enter the candy Id");
            string id = Console.ReadLine();
                store.Identification(id);
            bool validId = store.ValidId;
            if (validId == false) // Exits instance if incorrect id is given.
                {
                    Console.WriteLine("Please enter a valid Id");
                    MakeSale();
                }
            Console.WriteLine("Please enter the quantity you wish to purchase.");
                int qty = int.Parse(Console.ReadLine());
                store.SoldOut();
                store.SufficientStock(qty);
                store.PaymentCost(qty, id); //subtracting balance
                store.SufficientQty(qty, id);
            bool sufficientBalance = store.SufficientBalance; // check
            bool soldOut = store.IsOutOfStock;                //check
            bool sufficientQuantity = store.SufficientAmount; //check
            if (sufficientBalance == true && sufficientQuantity == true)
                {
                    store.AddToCart(qty, id); //removing from inv/adding to cart
                }
                else if (sufficientBalance == false)
                {
                    Console.WriteLine("Exceeded current balance.");
                }
                else if (soldOut == true)
                {
                    Console.WriteLine("Product is sold out.");
                }
                else if (sufficientQuantity == false)
                {
                    Console.WriteLine("The amount selected exceeds current stock amount. We are sorry.");
                }
            }
        private void CompleteSale()
        {
            List<Item_Candy> cart = store.cart;
            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("Qty  Name                 Description             Price   TotalCost");
            Console.WriteLine("-------------------------------------------------------------------");
            foreach (Item_Candy item in cart)
            {
                Console.WriteLine($"{item.Qty.ToString().PadRight(4)} {item.Name.PadRight(20)} {item.Type.PadRight(25)} ${item.Price.ToString().PadRight(6)} {item.TotalCost:C2}");
            }
            Console.WriteLine();
            Console.WriteLine($"Total: {store.ItemTotal():C2}");
            store.ChangeAmount(); //executes change amount
            Console.WriteLine();
            Console.WriteLine($"Change: {store.Change:C2}");
            Console.WriteLine(store.BillChange(store.Change));

        }
        private void DisplayMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Please enter a choice");
            Console.WriteLine("(1) Show Inventory");
            Console.WriteLine("(2) Make Sale");
            Console.WriteLine("(3) Quit");
        }

        private void DisplaySubMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Please enter a choice");
            Console.WriteLine("(1) Take Money");
            Console.WriteLine("(2) Select Products");
            Console.WriteLine("(3) Complete Sale");
        }

        private void DisplaySubTakeMoney()
        {
            Console.WriteLine();
            Console.WriteLine("Please enter the amount to add to balance");
            Console.WriteLine("(1) $1.00 ");
            Console.WriteLine("(2) $5.00");
            Console.WriteLine("(3) $10.00");
            Console.WriteLine("(4) $20.00");
            Console.WriteLine("(5) $50.00");
            Console.WriteLine("(6) $100.00");
            Console.WriteLine("(7) Back");


        }
    }
}