﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Linq;

namespace Capstone.Classes
{
    public class FileIO
    {
        private string file = "c:\\Store\\inventory.csv";
        private string Log = "C:\\Store\\Log.txt";
        public List<Item_Candy> ReadInventory()
        {
            List<Item_Candy> inventory = new List<Item_Candy>();

            using (StreamReader sr = new StreamReader(file))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    string[] lineArray = line.Split("|");
                    Item_Candy itemList = new Item_Candy(lineArray[0], lineArray[1], lineArray[2],
                       lineArray[4], decimal.Parse(lineArray[3]), 100, 0);
                    inventory.Add(itemList);


                }

            }
            return inventory;

        }
        public void WriteLog(string name)
        {
            using(StreamWriter sw = new StreamWriter(Log,true)) 
            {
                sw.WriteLine(name);
            }
        }
    }
}
