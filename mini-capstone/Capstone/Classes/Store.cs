﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Capstone.Classes
{
    /// <summary>
    /// Most of the "work" (the data and the methods) of dealing with inventory and money 
    /// should be created or controlled from this class
    /// </summary>
    public class Store
    {
        public bool SufficientBalance { get; set; } = true;
        public bool ExceedsBalance { get; set; } = false;
        public decimal Balance { get; set; } = 0;
        private FileIO fileaccess = new FileIO();
        public List<Item_Candy> inventory = new List<Item_Candy>();
        public bool ValidId { get; set; } = true;
        public bool IsOutOfStock { get; set; } = false;
        public bool SufficientAmount { get; set; } = true;
        public List<Item_Candy> cart = new List<Item_Candy>();
        public decimal Change { get;set; } = 0;
        public Store()
        {
            inventory = fileaccess.ReadInventory();     //allows the outside file to be read once.
        }
        public Item_Candy[] InventoryArray() // turns list into an array to be displayed
        {

            foreach(Item_Candy item in inventory) 
            {
                if (item.Wrapper == "T")                        //converts T to Y || F to N
                {
                   item.Wrapper = item.Wrapper.Replace("T", "Y");
                }
                else if (item.Wrapper == "F") 
                {
                   item.Wrapper = item.Wrapper.Replace("F", "N");
                }
                if (item.Type == "CH")
                {
                    item.Type = item.Type.Replace("CH","Chocolate Confectionery"); //replaces the type to a description
                }
                else if (item.Type == "HC")
                {
                    item.Type = item.Type.Replace("HC","Hard Tack Confectionery");
                }
                else if (item.Type == "LI")
                {
                    item.Type = item.Type.Replace("LI","Licorce and Jellies");
                }
                else if (item.Type == "SR")
                {
                    item.Type = item.Type.Replace("SR","Sour Flavored Candies");
                }
            }
            return inventory.ToArray();
        }
        public bool SoldOut() // checks if its sold out
        {
            foreach (Item_Candy item in inventory)
            {
                if (item.Qty == 0)
                {
                    IsOutOfStock = true;
                }
            }
            return IsOutOfStock;
        }

        public bool Identification(string id) // validates the list id to userinput
        {
            foreach (Item_Candy item in inventory)
            {
                if (item.Id == id)
                {
                    return ValidId = true;
                }
                else
                {
                    ValidId = false;
                }

            }
            return ValidId;
        }
        public List<Item_Candy> AddToCart(int qty,string id)
        {
            foreach (Item_Candy item in inventory)
            {
                if (item.Id == id)
                {
                    if (qty <= item.Qty)
                    { item.Qty = item.Qty - qty; } //removing quantity from current inventory

                    Item_Candy newItem = new Item_Candy();
                    newItem.Id = item.Id;
                    newItem.Name = item.Name;
                    newItem.Price = item.Price;
                    newItem.Wrapper = item.Wrapper;
                    newItem.Type = item.Type;
                    newItem.Qty = qty;
                    cart.Add(newItem);  //adding quantity/item to checkout cart.
                    fileaccess.WriteLog($"{DateTime.Today} {newItem.Qty} {newItem.Name} {newItem.Id} {newItem.TotalCost:C2} {Balance:C2}");
                }

            }
            return cart;
        }
        public bool SufficientStock(int qty) // validates if there is sufficient stock.
        {
            foreach (Item_Candy item in inventory)
            {
                if (qty > item.Qty)
                {
                    SufficientAmount = false;
                }
                else
                {
                    SufficientAmount = true;
                }
            }
            return SufficientAmount;
        }
        public bool SufficientQty(int qty,string id) //Does a sufficient check
        {
            foreach (Item_Candy item in inventory)
            {
                if (item.Id == id)
                {
                    if (item.Price * qty > Balance)
                    {
                        SufficientBalance = false;
                    }
                    else
                    {
                        SufficientBalance = true;
                    }
                }
            }
            return SufficientBalance;
        }
        public decimal Add1() //Gives money
        {
            if (Balance <= 999)
            {
                ExceedsBalance = false;
                Balance += 1;
                fileaccess.WriteLog($"{DateTime.Today} MONEY RECIEVED: $1.00 {Balance:C2}");
            }
            else
            {
                ExceedsBalance = true;
            }
            return Balance;

        }
        public decimal Add5()
        {
            if (Balance <= 995)
            {
                ExceedsBalance = false;
                Balance += 5;
                fileaccess.WriteLog($"{DateTime.Today} MONEY RECIEVED: $5.00 {Balance:C2}");
            }
            else
            {
                ExceedsBalance = true;
            }
            return Balance;
        }
        public decimal Add10()
        {
            if (Balance <= 990)
            {
                ExceedsBalance = false;
                Balance += 10;
                fileaccess.WriteLog($"{DateTime.Today} MONEY RECIEVED: $10.00 {Balance:C2}");
            }
            else
            {
                ExceedsBalance = true;
            }
            return Balance;
        }
        public decimal Add20()
        {
            if (Balance <= 980)
            {
                ExceedsBalance = false;
                Balance += 20;
                fileaccess.WriteLog($"{DateTime.Today} MONEY RECIEVED: $20.00 {Balance:C2}");
            }
            else
            {
                ExceedsBalance = true;
            }
            return Balance;
        }
        public decimal Add50()
        {
            if (Balance <= 950)
            {
                ExceedsBalance = false;
                Balance += 50;
                fileaccess.WriteLog($"{DateTime.Today} MONEY RECIEVED: $50.00 {Balance:C2}");
            }
            else
            {
                ExceedsBalance = true;
            }
            return Balance;
        }
        public decimal Add100()
        {
            if (Balance <= 900)
            {
                ExceedsBalance = false;
                Balance += 100;
                fileaccess.WriteLog($"{DateTime.Today} MONEY RECIEVED: $100.00 {Balance:C2}");
            }
            else
            {
                ExceedsBalance = true;
            }
            return Balance;
        }
        public decimal PaymentCost(int qty, string id) //subtracts item price from balance.
        {
            foreach (Item_Candy item in inventory)
            {
                if (item.Id == id)
                {
                    if ((item.Price * qty) <= Balance)
                    {
                        Balance -= item.Price * qty;
                    }
                    else if ((item.Price * qty) > Balance)
                    {
                        SufficientBalance = false;
                    }
                    
                }
            }
            return Balance;
        }
        public decimal ItemTotal() //Gives total amount of cart.
        {
            decimal Total = 0;
           foreach(Item_Candy item in cart)
            {
                Total += item.TotalCost;
            }
           return Total;
        }
        public decimal ChangeAmount()   //calculates change after sale and return balance to 0.
        {
            Change = Balance;
            Balance = 0;
            fileaccess.WriteLog($"CHANGE GIVEN: {Change:C2} {Balance:C2}"); //Logs change given
            return Change;
        }
        public string BillChange(decimal change)    //calculates the physical change to be given
        {   
            int twenty = 0;
            int ten = 0;
            int five = 0;   
            int one = 0;
            int quarter = 0;
            int dime = 0;
            int nickel = 0;
            int penny = 0;
            while ((change - 20) >= 0)
            {
                change -= 20;
                twenty++;
            }
            while ((change - 10)  >= 0)
            {
                change -= 10;
                ten++;
            }
            while ((change - 5) >= 0)
            {
                change -= 5;
                five++;
            }
            while (change - 1 >= 0)
            {
                change -= 1;
                one++;
            }
            while ((change - .25M)  >= 0)
            {
                change -= .25m;
                quarter++;
            }
            while ((change - .10M)  >= 0) 
            {
                change -= .10M;
                dime++; 
            }
            while ((change - .05M) >= 0)
            {
                change -= .05M;
                nickel++;
            }
            while ((change - .01M) >= 0)
            {
                change -= .01M;
                penny++;
            }
            return $"({twenty}) Twenties, ({ten}) Tens, ({five}) Fives, ({one}) Ones, ({quarter}) Quarters, ({dime}) Dimes, ({nickel}) Nickels, ({penny}) Pennies";
        }

    }
    }

    






